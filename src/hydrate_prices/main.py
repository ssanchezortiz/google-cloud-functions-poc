import base64
from google.cloud import storage
from google.cloud import pubsub_v1


def hydrate_prices(event, context):

    correlation_id, country = _get_event_content(event)
    _log("Function hydrate_prices triggered for {}".format(country), correlation_id)

    products = _read_products(correlation_id)
    _populate_prices(products)
    _store_products(products, correlation_id)

    _notify_hydration_completed(country, correlation_id)


def _get_event_content(event):
    pubsub_message = base64.b64decode(event['data']).decode('utf-8')
    return pubsub_message.split(",")


def _read_products(correlation_id):

    storage_client = storage.Client()

    bucket_name = "joyce-merchfeeds"
    bucket = storage_client.get_bucket(bucket_name)
    _log("Bucked obtained", correlation_id)

    blob = bucket.blob("{}/products".format(correlation_id))
    blob_content = blob.download_as_string()
    blob_lines = blob_content.decode("utf-8").split("\n")
    products = [ProductWithPrice(s.split(",")[0]) for s in blob_lines]
    _log("{} products read".format(len(products)), correlation_id)

    return products


def _populate_prices(products):
    for p in products:
        p.price = (150 * int(p.id))
    return products


def _store_products(products, correlation_id):

    storage_client = storage.Client()

    bucket_name = "joyce-merchfeeds"
    bucket = storage_client.get_bucket(bucket_name)
    _log("Bucked obtained", correlation_id)

    blob = bucket.blob("{}/products_with_prices".format(correlation_id))
    file_data = "\n".join([str(p) for p in products])
    _log(file_data, correlation_id)
    blob.upload_from_string(file_data)
    _log("Products with URLs stored", correlation_id)


def _notify_hydration_completed(country, correlation_id):
    publisher = pubsub_v1.PublisherClient()

    message = "{},{}".format(correlation_id,country)
    project_id = "channel-technology"
    topic_hydration_completed = "joyce-merchfeeds-hydration-completed"

    publisher.publish("projects/{}/topics/{}".format(project_id, topic_hydration_completed), message.encode())
    _log("Notified hydration completion", correlation_id)


def _log(message, correlation_id):
    print("[{}] - {}".format(correlation_id, message))


class ProductWithPrice:

    def __init__(self, id):
        self.id = id
        self.price = 0

    def __str__(self):
        return "{},{}".format(self.id, self.price)
provider "google" {
  credentials = "${file("account.json")}"
  project = "channel-technology"
  region  = "us-central1"
  zone    = "us-central1-c"
}

# =< PACKAGING >=

resource "google_storage_bucket" "joyce-deployment-bucket" {
  name = "joyce-deployment-bucket"
}
data "archive_file" "generate_catalog_zip" {
  type        = "zip"
  output_path = "${path.module}/bin/generate_catalog.zip"
  source {
    content  = "${file("${path.module}/src/generate_catalog/main.py")}"
    filename = "main.py"
  }
  source {
    content  = "${file("${path.module}/src/generate_catalog/requirements.txt")}"
    filename = "requirements.txt"
  }
}
resource "google_storage_bucket_object" "generate_catalog_archive" {
  name   = "generate_catalog.zip"
  bucket = "${google_storage_bucket.joyce-deployment-bucket.name}"
  source = "${path.module}/bin/generate_catalog.zip"
  depends_on = ["data.archive_file.generate_catalog_zip"]
}
data "archive_file" "hydrate_prices_zip" {
  type        = "zip"
  output_path = "${path.module}/bin/hydrate_prices.zip"
  source {
    content  = "${file("${path.module}/src/hydrate_prices/main.py")}"
    filename = "main.py"
  }
  source {
    content  = "${file("${path.module}/src/hydrate_prices/requirements.txt")}"
    filename = "requirements.txt"
  }
}
resource "google_storage_bucket_object" "hydrate_prices_archive" {
  name   = "hydrate_prices.zip"
  bucket = "${google_storage_bucket.joyce-deployment-bucket.name}"
  source = "${path.module}/bin/hydrate_prices.zip"
  depends_on = ["data.archive_file.hydrate_prices_zip"]
}
data "archive_file" "hydrate_urls_zip" {
  type        = "zip"
  output_path = "${path.module}/bin/hydrate_urls.zip"
  source {
    content  = "${file("${path.module}/src/hydrate_urls/main.py")}"
    filename = "main.py"
  }
  source {
    content  = "${file("${path.module}/src/hydrate_urls/requirements.txt")}"
    filename = "requirements.txt"
  }
}
resource "google_storage_bucket_object" "hydrate_urls_archive" {
  name   = "hydrate_urls.zip"
  bucket = "${google_storage_bucket.joyce-deployment-bucket.name}"
  source = "${path.module}/bin/hydrate_urls.zip"
  depends_on = ["data.archive_file.hydrate_urls_zip"]
}
data "archive_file" "hydration_completed_zip" {
  type        = "zip"
  output_path = "${path.module}/bin/hydration_completed.zip"
  source {
    content  = "${file("${path.module}/src/hydration_completed/main.py")}"
    filename = "main.py"
  }
  source {
    content  = "${file("${path.module}/src/hydration_completed/requirements.txt")}"
    filename = "requirements.txt"
  }
}
resource "google_storage_bucket_object" "hydration_completed_archive" {
  name   = "hydration_completed.zip"
  bucket = "${google_storage_bucket.joyce-deployment-bucket.name}"
  source = "${path.module}/bin/hydration_completed.zip"
  depends_on = ["data.archive_file.hydration_completed_zip"]
}

# =< DEPLOYMENT >=

# ===< Storage >===
resource "google_storage_bucket" "joyce-merchfeeds" {
  name     = "joyce-merchfeeds"
}

# ===< Pub/Sub topics >===
resource "google_pubsub_topic" "joyce-merchfeeds-generate-catalog" {
  name = "joyce-merchfeeds-generate-catalog"
}
resource "google_pubsub_topic" "joyce-merchfeeds-hydrate-prices" {
  name = "joyce-merchfeeds-hydrate-prices"
}
resource "google_pubsub_topic" "joyce-merchfeeds-hydrate-urls" {
  name = "joyce-merchfeeds-hydrate-urls"
}
resource "google_pubsub_topic" "joyce-merchfeeds-hydration-completed" {
  name = "joyce-merchfeeds-hydration-completed"
}

# ===< Functions >===
resource "google_cloudfunctions_function" "joyce-merchfeeds-generate-catalog" {
  name = "joyce-merchfeeds-generate-catalog"
  runtime = "python37"
  entry_point = "generate_catalog"
  event_trigger {
    event_type = "providers/cloud.pubsub/eventTypes/topic.publish"
    resource = "joyce-merchfeeds-generate-catalog"
  }
  source_archive_bucket = "${google_storage_bucket.joyce-deployment-bucket.name}"
  source_archive_object = "${google_storage_bucket_object.generate_catalog_archive.name}"
}
resource "google_cloudfunctions_function" "joyce_merchfeeds_hydrate_prices" {
  name = "joyce_merchfeeds_hydrate_prices"
  runtime = "python37"
  entry_point = "hydrate_prices"
  event_trigger {
    event_type = "providers/cloud.pubsub/eventTypes/topic.publish"
    resource = "joyce-merchfeeds-hydrate-prices"
  }
  source_archive_bucket = "${google_storage_bucket.joyce-deployment-bucket.name}"
  source_archive_object = "${google_storage_bucket_object.hydrate_prices_archive.name}"
}
resource "google_cloudfunctions_function" "joyce_merchfeeds_hydrate_urls" {
  name = "joyce_merchfeeds_hydrate_urls"
  runtime = "python37"
  entry_point = "hydrate_urls"
  event_trigger {
    event_type = "providers/cloud.pubsub/eventTypes/topic.publish"
    resource = "joyce-merchfeeds-hydrate-urls"
  }
  source_archive_bucket = "${google_storage_bucket.joyce-deployment-bucket.name}"
  source_archive_object = "${google_storage_bucket_object.hydrate_urls_archive.name}"
}
resource "google_cloudfunctions_function" "joyce_merchfeeds_hydration_completed" {
  name = "joyce_merchfeeds_hydration_completed"
  runtime = "python37"
  entry_point = "hydration_completed"
  event_trigger {
    event_type = "providers/cloud.pubsub/eventTypes/topic.publish"
    resource = "joyce-merchfeeds-hydration-completed"
  }
  source_archive_bucket = "${google_storage_bucket.joyce-deployment-bucket.name}"
  source_archive_object = "${google_storage_bucket_object.hydration_completed_archive.name}"
}
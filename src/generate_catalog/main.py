import base64
import uuid
from google.cloud import storage
from google.cloud import pubsub_v1


def generate_catalog(event, context):

    correlation_id = str(uuid.uuid4())
    _log("Function generate_catalog triggered", correlation_id)

    country = _get_country(event)
    products = _get_products(country, correlation_id)
    _store_products(products, correlation_id)
    _trigger_prods_hydration(country, correlation_id)


def _get_country(event):
    """Triggered from a message on a Cloud Pub/Sub topic.
    Args:
         event (dict): Event payload.
         context (google.cloud.functions.Context): Metadata for the event.
    """
    pubsub_message = base64.b64decode(event['data']).decode('utf-8')
    return pubsub_message


def _get_products(country, correlation_id):
    _log("Fetching products for {}".format(country), correlation_id)
    products = [Product(1, "Product 1"), Product(2, "Product 2"), Product(3, "Product 3"), Product(4, "Product 4")]
    return products


def _store_products(products, correlation_id):

    storage_client = storage.Client()

    bucket_name = "joyce-merchfeeds"
    bucket = storage_client.get_bucket(bucket_name)
    _log("Bucked obtained", correlation_id)

    blob = bucket.blob("{}/products".format(correlation_id))
    file_data = "\n".join([str(p) for p in products])
    blob.upload_from_string(file_data)
    _log("Products stored", correlation_id)


def _trigger_prods_hydration(country, correlation_id):
    publisher = pubsub_v1.PublisherClient()

    message = "{},{}".format(correlation_id,country)
    project_id = "channel-technology"
    topic_hydrate_urls = "joyce-merchfeeds-hydrate-urls"
    topic_hydrate_prices = "joyce-merchfeeds-hydrate-prices"

    publisher.publish("projects/{}/topics/{}".format(project_id, topic_hydrate_urls), message.encode())
    _log("Triggered URL hydration", correlation_id)

    publisher.publish("projects/{}/topics/{}".format(project_id, topic_hydrate_prices), message.encode())
    _log("Triggered prices hydration", correlation_id)


def _log(message, correlation_id):
    print("[{}] - {}".format(correlation_id, message))


class Product:

    def __init__(self, id, name):
        self.id = id
        self.name = name

    def __str__(self):
        return "{},{}".format(self.id, self.name)
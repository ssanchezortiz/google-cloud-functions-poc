import base64
from google.cloud import storage


def hydration_completed(event, context):

    correlation_id, country = _get_event_content(event)
    _log("Function hydration_completed triggered for {}".format(country), correlation_id)

    try:
        bucket = _obtain_bucket(correlation_id)
        prods, prods_with_price, prods_with_url = _read_products(bucket, correlation_id)
        catalog_products = [CatalogProduct(p, pwp, pwu) for p, pwp, pwu in zip(prods, prods_with_price, prods_with_url)]

        _store_catalog(catalog_products, bucket, country, correlation_id)
        _log("Catalog created for {}".format(country), correlation_id)

    except AssertionError as err:
        _log("File {} not ready".format(err.args[0]), correlation_id)


def _obtain_bucket(correlation_id):
    storage_client = storage.Client()

    bucket_name = "joyce-merchfeeds"
    bucket = storage_client.get_bucket(bucket_name)
    _log("Bucked obtained", correlation_id)
    return bucket


def _get_event_content(event):
    pubsub_message = base64.b64decode(event['data']).decode('utf-8')
    return pubsub_message.split(",")


def _read_products(bucket, correlation_id):

    filename_prods = "products"
    filename_prods_with_price = "products_with_prices"
    filename_prods_with_url = "products_with_urls"

    assert_all_files_are_available(bucket, [filename_prods, filename_prods_with_price, filename_prods_with_url], correlation_id)

    prods_as_str_arr = _fetch_blob_content_as_string_array(bucket, filename_prods, correlation_id)
    products = [Product(s.split(",")[0], s.split(",")[1]) for s in prods_as_str_arr]
    _log("{} products read".format(len(products)), correlation_id)

    prods_with_price_as_str_arr = _fetch_blob_content_as_string_array(bucket, filename_prods_with_price, correlation_id)
    products_with_price = [ProductWithPrice(s.split(",")[0], int(s.split(",")[1])) for s in prods_with_price_as_str_arr]
    _log("{} products with price read".format(len(products_with_price)), correlation_id)

    prods_with_url_as_str_arr = _fetch_blob_content_as_string_array(bucket, filename_prods_with_url, correlation_id)
    products_with_url = [ProductWithUrl(s.split(",")[0], s.split(",")[1]) for s in prods_with_url_as_str_arr]
    _log("{} products with Url read".format(len(products_with_url)), correlation_id)

    return products, products_with_price, products_with_url


def _fetch_blob_content_as_string_array(bucket, filename, correlation_id):

    blob = bucket.blob("{}/{}".format(correlation_id, filename))
    blob_content = blob.download_as_string()
    blob_lines = blob_content.decode("utf-8").split("\n")
    return blob_lines


def assert_all_files_are_available(bucket, files, correlation_id):
    for file in files:
        if bucket.get_blob("{}/{}".format(correlation_id, file)) is None:
            raise AssertionError(file)


def _store_catalog(products, bucket, country, correlation_id):

    blob = bucket.blob("{}/catalog_{}".format(correlation_id, country))
    file_data = "\n".join([str(p) for p in products])
    blob.upload_from_string(file_data)
    _log("Catalog created for country {}".format(country), correlation_id)


def _log(message, correlation_id):
    print("[{}] - {}".format(correlation_id, message))


class Product:
    def __init__(self, id, name):
        self.id = id
        self.name = name


class ProductWithPrice:
    def __init__(self, id, price):
        self.id = id
        self.price = price


class ProductWithUrl:
    def __init__(self, id, url):
        self.id = id
        self.url = url


class CatalogProduct:
    def __init__(self, prod, prod_with_price, prod_with_url):
        self.id = prod.id
        self.name = prod.name
        self.price = prod_with_price.price
        self.url = prod_with_url.url

    def __str__(self):
        return "{},{},{},{}".format(self.id, self.name, self.price, self.url)

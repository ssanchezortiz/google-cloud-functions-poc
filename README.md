# google-cloud-functions-poc

POC for a simple example on how to use Pub/Sub messages to trigger and orchestrate parallel jobs and stitch the results in a final consolidated file. 

## Deploy and Run

### Deploy
- Install Terraform (0.12 used at the time of writting this doc)
- Replace the content in `account.json` with the content of the json key generated in the [Google Cloud Platform console](https://console.cloud.google.com), under `Google Cloud Platform / IAM & Admin / Service accounts`
- In the root dir of this project, run:
    - `${terraform_path}/terraform init`
    - `${terraform_path}/terraform apply`

### Run
- Open the [Google Cloud Platform console](https://console.cloud.google.com)
- Click on the **Pub/Sub** left side menu option
- Open the topic named `projects/channel-technology/topics/joyce-merchfeeds-generate-catalog`
- Click on the **PUBLISH MESSAGE** top menu option
- Enter some message in the free text box, and click **Publish**

This will trigger the first handler in the flow (see more details below).

## Infrastructure

project = `channel-technology`

### Storage
bucket = `joyce-merchfeeds`

Inside the bucket, a new *folder* is created for each new execution, named after the `correlation_id` associated to the execution. 

### Topics
- `projects/channel-technology/topics/joyce-merchfeeds-generate-catalog`
- `projects/channel-technology/topics/joyce-merchfeeds-hydrate-prices`
- `projects/channel-technology/topics/joyce-merchfeeds-hydrate-urls`
- `projects/channel-technology/topics/joyce-merchfeeds-hydration-completed`

### Functions
- `joyce_merchfeeds_generate_catalog`
- `joyce_merchfeeds_hydrate_prices`
- `joyce_merchfeeds_hydrate_urls`
- `joyce_merchfeeds_hydration_completed`


## Flow

1. Publish a message in the `joyce-merchfeeds-generate-catalog` Pub/Sub topic to trigger the process
2. The function `joyce_merchfeeds_generate_catalog` bound to the topic gets triggered, generates a seed file, and publishes messages into `joyce-merchfeeds-hydrate-prices` and `joyce-merchfeeds-hydrate-urls` to run their bound functions in parallel
3. Each intermediate function (`joyce_merchfeeds_hydrate_prices` and `joyce_merchfeeds_hydrate_urls`) does its magic, generates an intermediate file in the execution bucket, and publishes a message (each) into `joyce-merchfeeds-hydration-completed` once done
4. For each message `joyce_merchfeeds_hydration_completed` gets executed and evaluates if all required files are in place, and if so creates the final consolidated file with all info stitched together.



